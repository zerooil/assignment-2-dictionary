ASM = nasm -f elf64

all: main.o lib.o dict.o
	@echo "Building program..."
	@ld main.o lib.o dict.o -o main

lib.o: lib.asm lib.inc
	@echo "Building lib..."
	@$(ASM) lib.asm -o lib.o

dict.o: dict.asm dict.inc
	@echo "Building dictionary lib..."
	@$(ASM) dict.asm -o dict.o

main.o: main.asm words.inc colon.inc
	@echo "Building main..."
	@$(ASM) main.asm -o main.o

clean:
	@echo "Cleaning..."
	@rm -f *.o

test:
	@python3 test.py
.PHONY:all clean test
