%define prev 0
%macro colon 2
    %ifstr %1             ; проверка ключа
      %ifid %2            ; проверка метки
          %2: dq prev     ; метка: предыдущая вхождение
          db %1, 0        ; ключ
          %define prev %2
      %else
          %error "Label should be an id"
      %endif
    %else
          %error "Key should be a string"
    %endif
%endmacro
%define KEY_OFFSET 8
; значение
