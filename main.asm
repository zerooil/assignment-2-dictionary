%include "words.inc"
%include "dict.inc"
%include "lib.inc"
%define BUFFER_SIZE 256

global _start

section .rodata
  readError: db "Key is too long", 0
  notFound: db "Key not found", 0

section .bss
  buffer: resb BUFFER_SIZE  ; определяем буффер размера BUFFER_SIZE

section .text
_start:
  mov rdi, buffer
  mov rsi, BUFFER_SIZE      ; читаем слово в буффер
  call read_line
  test rax, rax             ; проверяем на переполнение
  je .read_error

  mov rdi, rax
  mov rsi, first_word
  call find_word            ; ищем ключ по словарю
  test rax, rax             ; такой ключ есть?
  je .not_found

  mov rdi, rax
  lea rdi, [rdi+KEY_OFFSET] ; находим положение ключа
  push rdi
  call string_length        ; находим длину ключа
  pop rdi
  lea rdi, [rdi+rax+1]      ; находим положение значения
  call print_string         ; выводим значение
  jmp .exit

.not_found:
  mov rdi, notFound
  call print_error          ; печатаем ошибку о ненахождении ключа
  jmp .exit
.read_error:
  mov rdi, readError        ; печатаем ошибку о слишком длинном считанном ключе
  call print_error
.exit:
  call print_newline
  call exit
